<?php

/**
 * @file
 * Timepad Module API File.
 */

/**
 * Class TimePadApi.
 */
class TimePadApi {
  /**
   * URL for API.
   */
  const URL = 'https://api.timepad.ru/';

  /**
   * Api version.
   */
  const VERSION = 'v1';

  /**
   * Auth URL for API.
   */
  const AUTH_URL = 'https://api.timepad.ru/oauth/authorize';

  /**
   * HTTP methods.
   */
  const METHOD_GET = 'GET';
  const METHOD_POST = 'POST';

  /**
   * Curl instance.
   *
   * @var curl
   */
  protected $curl;

  /**
   * Authorize token, for protected API methods.
   *
   * @var token
   */
  protected $token;

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->token = variable_get('timepad_access_token', '');
  }

  /**
   * Class destructor.
   */
  public function __destruct() {
    curl_close($this->curl);
  }

  /**
   * Generate url for request.
   *
   * @param string $path
   *   Protected function getUrl $path.
   * @param string $base
   *   Protected function getUrl $base.
   *
   * @return string
   *   Protected function getUrl string.
   */
  protected function getUrl($path, $base = '') {
    $base = $base ? $base : self::URL;
    return $base . $path;
  }

  /**
   * Get Events List.
   *
   * @param array $params
   *   Public function eventsGetList array params.
   *
   * @return array
   *   Public function eventsGetList array.
   *
   * @access public
   */
  public function eventsGetList(array $params = array()) {
    try {
      $result = $this->entityAction($params, self::METHOD_GET, 'events');
    }
    catch (Exception $e) {
      if ($e->getCode() == 404) {
        // Event not found.
        $result = FALSE;
      }
      else {
        throw $e;
      }
    }

    return $result;
  }

  /**
   * Create Event.
   *
   * @param array $params
   *   Public function eventsCreate params.
   *
   * @return array
   *   Public function eventsCreate array.
   *
   * @access public
   */
  public function eventsCreate(array $params) {
    return $this->entityAction($params, self::METHOD_POST, 'events');
  }

  /**
   * Edit Event.
   *
   * @param int $event_id
   *   Public function eventsEdit event_id.
   * @param array $params
   *   Public function eventsEdit array params.
   *
   * @return array
   *   Public function eventsEdit array.
   *
   * @access public
   */
  public function eventsEdit($event_id, array $params) {
    return $this->entityAction($params, self::METHOD_POST, 'events', $event_id);
  }

  /**
   * Get Event by it identifier.
   *
   * @param int $event_id
   *   Public function eventsGet event_id.
   * @param array $params
   *   Public function eventsGet array params.
   *
   * @return array|bool
   *   Public function eventsGet array bool.
   *
   * @throws \Exception
   *
   * @access public
   */
  public function eventsGet($event_id, array $params = array()) {
    try {
      $result = $this->entityAction($params, self::METHOD_GET, 'events', $event_id);
    }
    catch (Exception $e) {
      if ($e->getCode() == 404) {
        // Event not found.
        $result = FALSE;
      }
      else {
        throw $e;
      }
    }

    return $result;
  }

  /**
   * Create organization.
   *
   * @param array $params
   *   Public function organizationsCreate array params.
   *
   * @return array
   *   Public function organizationsCreate array.
   *
   * @throws Exception.
   */
  public function organizationsCreate(array $params) {
    return $this->entityAction($params, self::METHOD_POST, 'organizations');
  }

  /**
   * Makes an action (Create / Save/ Delete) with any timepad entity.
   *
   * @param array $params
   *   Array with params to operate.
   * @param string $method
   *   Private function entityAction string method.
   *
   * @throws Exception
   */
  private function entityAction(array $params, $method) {
    // Get args to format URL.
    $url_parts = func_get_args();
    // First argument will be "params", we don't need it this way.
    $url_parts[0] = self::VERSION;
    // Remove $method argument.
    unset($url_parts[1]);

    // Format URL form all other arguments.
    $url = self::URL . implode('/', $url_parts);

    return $this->curlRequest(
      $url,
      $method,
      $params,
      array(
        'Authorization: Bearer ' . $this->token,
      )
    );
  }

  /**
   * Get information about user account.
   *
   * @return array
   *   Public function accountGet.
   */
  public function accountGet() {
    $params = array(
      'token' => $this->token,
    );

    $url = self::URL . 'introspect';

    $result = $this->curlRequest($url, self::METHOD_GET, $params);

    return $result;
  }

  /**
   * Execution of the request.
   *
   * @param string $url
   *   Protected function curlRequest url.
   * @param string $method
   *   Protected function curlRequest string method.
   * @param string $parameters
   *   Protected function curlRequest string parameters.
   * @param array $headers
   *   Protected function curlRequest array headers.
   * @param int $timeout
   *   Protected function curlRequest int timeout.
   *
   * @return array
   *   Protected function curlRequest array.
   *
   * @access protected
   *
   * @throws \Exception
   */
  protected function curlRequest($url, $method = 'GET', $parameters = array(), array $headers = array(), $timeout = 30) {
    if ($method == self::METHOD_GET && $parameters) {
      $url .= '?' . http_build_query($parameters);
    }

    // Get curl handler or initiate it.
    if (!$this->curl) {
      $this->curl = curl_init();
    }

    // Set general arguments.
    curl_setopt($this->curl, CURLOPT_URL, $url);
    curl_setopt($this->curl, CURLOPT_FAILONERROR, FALSE);
    curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($this->curl, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($this->curl, CURLOPT_HEADER, FALSE);

    // Reset some arguments, in order to avoid use some from previous request.
    curl_setopt($this->curl, CURLOPT_POST, FALSE);

    curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);

    if ($method == self::METHOD_POST && $parameters) {
      curl_setopt($this->curl, CURLOPT_POST, TRUE);

      // Encode parameters if them already not encoded in json.
      if (!is_string($parameters)) {
        $parameters = http_build_query($parameters);
      }

      curl_setopt($this->curl, CURLOPT_POSTFIELDS, $parameters);
    }

    $response = curl_exec($this->curl);
    $statusCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

    $errno = curl_errno($this->curl);
    $error = curl_error($this->curl);

    if ($errno) {
      throw new Exception($error, $errno);
    }

    $result = drupal_json_decode($response);

    if ($statusCode >= 400) {
      if (!empty($result['response_status']['errors'])) {
        foreach ($result['response_status']['errors'] as $error) {
          $message = '';
          if (!empty($error['error_code'])) {
            $message .= 'Error ' . $error['error_code'] . ': ';
          }
          if (!empty($error['message'])) {
            $message .= $error['message'] . ' ';
          }
          if (!empty($error['field_name'])) {
            $message .= $error['field_name'];
          }
          if (!empty($message)) {
            drupal_set_message($message, 'error');
          }
        }
      }
      throw new Exception($result['response_status']['message'], $statusCode);
    }

    return !empty($result) ? $result : array();
  }

  /**
   * Create Order.
   *
   * @param int $event_id
   *   Public function ordersCreate event_id.
   * @param array $params
   *   Public function ordersCreate array params.
   *
   * @return array
   *   Public function ordersCreate array.
   *
   * @access public
   */
  public function ordersCreate($event_id, array $params) {
    return $this->entityAction($params, self::METHOD_POST, 'events', $event_id, 'orders');
  }

  /**
   * Get Orders List.
   *
   * @param int $event_id
   *   Public function ordersGetList event_id.
   * @param array $params
   *   Public function ordersGetList array params.
   *
   * @return array
   *   Public function ordersGetList array.
   *
   * @access public
   */
  public function ordersGetList($event_id, array $params = array()) {
    try {
      $result = $this->entityAction($params, self::METHOD_GET, 'events', $event_id, 'orders');
    }
    catch (Exception $e) {
      if ($e->getCode() == 404) {
        // Event not found.
        $result = FALSE;
      }
      else {
        throw $e;
      }
    }

    return $result;
  }

  /**
   * Get Order by id.
   *
   * @param int $order_id
   *   Public function ordersGet order_id.
   * @param int $event_id
   *   Public function ordersGet event_id.
   * @param array $params
   *   Public function ordersGet array params.
   *
   * @return array
   *   Public function ordersGet array.
   *
   * @access public
   */
  public function ordersGet($order_id, $event_id, array $params = array()) {
    try {
      $result = $this->entityAction($params, self::METHOD_GET, 'events', $event_id, 'orders', $order_id);
    }
    catch (Exception $e) {
      if ($e->getCode() == 404) {
        // Event not found.
        $result = FALSE;
      }
      else {
        throw $e;
      }
    }

    return $result;
  }

  /**
   * Get Webhooks List.
   *
   * @param int $organization_id
   *   public function webhooksGetList int organization_id.
   * @param array $params
   *   public function webhooksGetList array params.
   *
   * @return array
   *   public function webhooksGetList array.
   *
   * @access view_private_organizations
   */
  public function webhooksGetList($organization_id, array $params = array()) {
    try {
      $result = $this->entityAction($params, self::METHOD_GET, 'organizations', $organization_id, 'hooks');
    }
    catch (Exception $e) {
      if ($e->getCode() == 404) {
        // Event not found.
        $result = FALSE;
      }
      else {
        throw $e;
      }
    }

    return $result;
  }

  /**
   * Create Webhook.
   *
   * @param int $organization_id
   *   Public function webhooksCreate int organization_id.
   * @param array $params
   *   Public function webhooksCreate array params.
   *
   * @return array
   *   Public function webhooksCreate array.
   *
   * @access edit_organizations_hooks
   */
  public function webhooksCreate($organization_id, array $params) {
    return $this->entityAction($params, self::METHOD_POST, 'organizations', $organization_id, 'hooks');
  }

  /**
   * Get Webhook by id.
   *
   * @param int $hook_id
   *   Public function webhooksGet int hook_id.
   * @param int $organization_id
   *   Public function webhooksGet int organization_id.
   * @param int $event_id
   *   Public function webhooksGet int event_id.
   * @param array $params
   *   Public function webhooksGet array params.
   *
   * @return array
   *   Public function webhooksGet array.
   *
   * @access view_private_organizations
   */
  public function webhooksGet($hook_id, $organization_id, array $params = array()) {
    try {
      $result = $this->entityAction($params, self::METHOD_GET, 'organizations', $organization_id, 'hooks', $hook_id);
    }
    catch (Exception $e) {
      if ($e->getCode() == 404) {
        // Event not found.
        $result = FALSE;
      }
      else {
        throw $e;
      }
    }

    return $result;
  }

  /**
   * Edit Webhook.
   *
   * @param int $hook_id
   *   Public function webhooksEdit int hook_id.
   * @param int $organization_id
   *   Public function webhooksEdit int organization_id.
   * @param array $params
   *   Public function webhooksEdit array params.
   *
   * @return array
   *   Public function webhooksEdit array.
   *
   * @access edit_organizations_hooks
   */
  public function webhooksEdit($hook_id, $organization_id, array $params) {
    return $this->entityAction($params, self::METHOD_POST, 'organizations', $organization_id, 'hooks', $hook_id);
  }

}
