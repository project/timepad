<?php

/**
 * @file
 * Example for create event.
 */

// Get all events from TimePad.
$client = new TimePadApi();
$params = array(
  'name' => 'string',
  // Date in format "Y-m-d\Th:i:sO".
  'starts_at' => '2015-09-26T06:21:14.243Z',
  // Date in format "Y-m-d\Th:i:sO".
  'ends_at' => '2015-09-26T06:21:14.243Z',
  'description_short' => 'string',
  'description_html' => 'string',
  'organization' => array(
    'id' => 0,
    // Name of your organization "example.timepad.ru".
    'subdomain' => 'example',
  ),
  'ticket_types' => array(
    array(
      'price' => 0,
      'name' => 'string',
      'description' => 'string',
    ),
  ),
  // For now it isn't worked.
  'questions' => array(),
  'categories' => array(
    array('name' => 'ИТ и интернет'),
  ),
  'location' => array(
    'city' => 'string',
    'address' => 'string',
  ),
  // URL to image.
  'poster_image_url' => 'string',
  'properties' => array(
    'string',
  ),
  'custom' => array(),
  // Can be public, private, draft, link_only.
  'access_status' => 'draft',
);
$result = $client->eventsCreate($params);
