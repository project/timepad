<?php

/**
 * @file
 * Example for create event.
 */

// Get all events from TimePad.
// For edit events token is necessary.
$token = '';
$client = new TimePadApi($token);
$params = array(
  'name' => 'Test edit',
  'description_short' => 'Test description',
  'description_html' => 'Test html description',
  'organization' => array(
    'id' => 0,
  // Name of your organization "example.timepad.ru".
    'subdomain' => 'example',
  ),
  'ticket_types' => array(
    array(
      'price' => 0,
      'name' => 'string',
      'description' => 'string',
    ),
  ),
  // For now it isn't worked.
  'questions' => array(),
  'categories' => array(
    array('name' => 'ИТ и интернет'),
  ),
  'location' => array(
    'city' => 'Москва',
    'address' => 'string',
  ),
  // URL to image.
  'poster_image_url' => 'string',
  'properties' => array(
    'string',
  ),
  'custom' => array(),
  'access_status' => 'draft',
);
$event_id = 0;
$result = $client->eventsEdit($event_id, $params);
