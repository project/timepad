<?php

/**
 * @file
 * Example for create organization in timepad.
 */

// Create new client.
$client = new TimePadApi();
// Set up params.
$params = array(
  // Organization name.
  'name' => 'string',
  // Subdomain for .timepad.com.
  'subdomain' => 'string',
  // Contact phone.
  'phone' => '+79555555441',
);

$result = $client->organizationsCreate($params);
